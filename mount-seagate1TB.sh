#!/bin/bash

DIR="/home/m_danylov/seagate1TB/"

echo 'Menu for mounting external hdd. Press ctrl+c to skip'
sudo fdisk -l
echo 'Choose device to mount: '
read DEVICE

# check if dir exists
if [ ! -d "$DIR" ]; then
  # Control will enter here if $DIRECTORY doesn't exist.
  mkdir $DIR
fi

#check if external hdd is mounted
if grep '/dev/$DEVICE' /etc/mtab > /dev/null 2>&1 ; then 
	echo "External hdd is already mounted"
else 
	#echo "External hdd is not mounted"
	echo "Mounting..."
	sudo mount /dev/$DEVICE $DIR
	echo "External hdd is successfuly mounted"
fi

alias exthdd="cd $DIR"
